// console.log("My name is Bhawana")
import express, { json } from "express"
import { firstRouter } from "./src/route/firstRouter.js"
import { traineesRouter } from "./src/route/traineesRouter.js"
import { vehicleRouter } from "./src/route/vehicleRouter.js"
import { learnRouter } from "./src/route/learnRouter.js"
import { studentRouter } from "./src/route/studentRouter.js"
import { homeworkRouter } from "./src/route/homeworkRouter.js"
import { contactRouter } from "./src/route/contactRouter.js"
import { schoolRouter } from "./src/route/schoolRouter.js"
import { connectToMongoDb } from "./src/connectToDb/connecttoMongoDb.js"
import { classRoomRouter } from "./src/route/classRoomRouter.js"
import { departmentRouter } from "./src/route/departmentRouter.js"
import { hostelRouter } from "./src/route/hostelRouter.js"
import { teacherRouter } from "./src/route/teacherRouter.js"
import { BookRouter } from "./src/route/bookRouter.js"
import { collegeRouter } from "./src/route/collegeRouter.js"
import { productRouter } from "./src/route/productRouter.js"
import { userRouter } from "./src/route/userRouter.js"
import { reviewRouter } from "./src/route/reviewRouter.js"
// import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { port, secretKey } from "./src/constant.js"
import { webUserRouter } from "./src/route/webUserRouter.js"
import { fileRouter } from "./src/route/fileRouter.js"



let expressApp = express() 
expressApp.use(express.static("./public"))
expressApp.use(json()) //imp
connectToMongoDb()

// expressApp.use((req,res,next)=>{
// console.log("I am application middleware")
// next()
// })

expressApp.use("/firsts", firstRouter)
expressApp.use("/schools",schoolRouter)
expressApp.use("/sanits",vehicleRouter)
expressApp.use("/trainees",traineesRouter)
expressApp.use("/learn",learnRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/homeworks",homeworkRouter)
expressApp.use("/contacts",contactRouter)
expressApp.use("/classrooms",classRoomRouter)
expressApp.use("/departments",departmentRouter)
expressApp.use("/hostels",hostelRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/books",BookRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/products",productRouter)
expressApp.use("/users",userRouter)
expressApp.use("/reviews",reviewRouter)
expressApp.use("/files",fileRouter)
expressApp.use("/web-users",webUserRouter)//kabaf case (-)
                    //port
expressApp.listen(port, () =>{
console.log("app is listening at port 8000")
})


/*
make backend application (using express)
attached port to that application
 */

// let password = "abc@1234"
// let hashPassword = await bcrypt.hash(password, 10)
// console.log(hashPassword)

// let hashPassword = "$2b$10$0uX29v.38Pxcvp9p2VvHq.DBK650VdhDhkwrtczuMXrBDzCdviUOK"
// let password = "abc@1234"
// let isPasswordMatch = await bcrypt.compare(password, hashPassword)
// console.log(isPasswordMatch)

//hash=> euta form bata aarko form ma convert ko lagi //secure
// compare=> check garni hashpassword ra password
/// hash password banaun ko lagi bcrypt.hash ko use garinx


//generate token //sessiontype // token xa vne profile herna milyo natra milena
//
//backend
// let infoObj={
    // name:"Bhawana",
    // age: 25,
    // address: "Dhumbarahi",

    // _id: "123456789",
// }

// let expiryInfo ={
//     expiresIn: "365d",
// }
// let token = jwt.sign(infoObj, secretKey, expiryInfo)
// console.log(token)


 //validate  token
// let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MDY5NzQ2ODYsImV4cCI6MTczODUxMDY4Nn0.8n2hlLOOK_kQpGW5WoekEyWOxUd94pdR6uSjmdxjtvk"
// try {
//     let infoObj = jwt.verify(token, "Dw11")
//     console.log(infoObj)
// } catch (error) {
//     console.log(error.message)
// }

//for token to be validate
//token must be made from the given secret key
//token must not  expire

//if token is valid it gives infoObj else error

//.env file

//send file from postman
//save file to our server 



