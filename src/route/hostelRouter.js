import { Router } from "express";
import { createHostel, deleteHostel, readHostel, readSpecificHostel, updateHostel } from "../controller/hostelController.js";

export let hostelRouter = Router()
hostelRouter
.route("/")
.post(createHostel)
.get(readHostel)

hostelRouter
.route("/:id")
.get(readSpecificHostel)
.patch(updateHostel)
.delete(deleteHostel)