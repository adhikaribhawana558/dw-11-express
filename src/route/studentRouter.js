import { Router } from "express";
import { Student } from "../schema/model.js";
import { createStudent, deleteStudent, getSpecificStudent, getStudent, updateStudent } from "../controller/studentController.js";


// url = localhost:8000/students  , method = post     res => {success:true, message:"Student create successfully"}
// url = localhost:8000/students  , method = get     res => {success:true, message:"Student Read successfully"}
// rl = localhost:8000/students/any  , method = get     res => {success:true, message:"Student Read successfully"}
// rl = localhost:8000/students/any , method = patch     res => {success:true, message:"Student Updated successfully"}
// rl = localhost:8000/students/any , method = delete     res => {success:true, message:"Student deleted successfully"}
export let studentRouter = Router()
studentRouter
.route("/")
.post(createStudent)

.get(getStudent) 


studentRouter 
.route("/:id")
.get(getSpecificStudent)

.patch(updateStudent)

.delete(deleteStudent)



