import { Router } from "express"
import { createProduct, deleteProduct, readProduct, readSpecificProduct, updateProduct } from "../controller/productController.js"

export let productRouter = Router()
productRouter
.route("/")
.post(createProduct)
.get(readProduct)

productRouter
.route("/:id")
.get(readSpecificProduct)
.patch(updateProduct)
.delete(deleteProduct)
