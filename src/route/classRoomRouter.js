import { Router } from "express";
import { createClassRoom, deleteClassRoom, readClassRoom, readSpecificClassRoom, updateClassRoom } from "../controller/classRoomController.js"; //.js=>

export let classRoomRouter = Router()
classRoomRouter
.route("/")
.post(createClassRoom)
.get(readClassRoom)

classRoomRouter
.route("/:id")
.get(readSpecificClassRoom)
.patch(updateClassRoom)
.delete(deleteClassRoom)
