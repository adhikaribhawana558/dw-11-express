import { Router } from "express";

export let firstRouter = Router()

firstRouter
.route("/")
.get((req,res,next)=> {
    console.log("I am middleware 1")
    let error = new Error("New Error")
    next(error)
    // next("abc")
},
(err,req,res,next)=>{
    console.log(err.message)
    console.log("I am error middleware")
    next()
},
(req,res,next)=>{
    console.log("I am middleware 2")
})
.post((req,res,next)=>{
    console.log("I am middleware 1")
    req.name = "bhawana"
    next()
},
(req,res,next)=>{
    console.log(req.name)
    console.log("I am middleware 2")
})
.patch(()=> {
    console.log("i am home patch")
})
.delete(()=> {
    console.log("i am home delete")
})

firstRouter
.route("/name")
.get(()=> {
    console.log("i am name get")
})
.post(()=> {
    console.log("i am name post")
})
.patch(()=> {
    console.log("i am name patch")
})
.delete(()=> {
    console.log("i am name delete")
})
/*
url:"localhost:8000", method: "get" => i am home get
*/


/* 
url:"localhost:8000/address", method:"get"   => i am address get

url:"localhost:8000/address", method:"post"  => i am address(post)
url:"localhost:8000/address", method:"patch"  => i am address(patch)
url:"localhost:8000/address", method:"delete" => i am address (delete)
 */
firstRouter
.route("/address")
.get(()=> {
    console.log("i am address get")
})
.post(()=> {
    console.log("i am address post")
})
.patch(()=> {
    console.log("i am address patch")
})
.delete(()=> {
    console.log("i am address delete")
})


/* 
url:"localhost:8000/age", method:"get"   => i am age get

url:"localhost:8000/age", method:"post"  => i am age(post)
url:"localhost:8000/age", method:"patch"  => i am age(patch)
url:"localhost:8000/age", method:"delete" => i am age (delete)
 */
firstRouter
.route("/age")
.get(()=> {
    console.log("i am age get")
})
.post(()=> {
    console.log("i am age post")
})
.patch(()=> {
    console.log("i am age patch")
})
.delete(()=> {
    console.log("i am age delete")
})


/* 
url:"localhost:8000/height", method:"get"   => i am height get

url:"localhost:8000/height", method:"post"  => i am height(post)
url:"localhost:8000/height", method:"patch"  => i am height(patch)
url:"localhost:8000/height", method:"delete" => i am height (delete)
 */

firstRouter
.route("/height")
.get(()=> {
    console.log("i am height get")
})
.post(()=> {
    console.log("i am height post")
})
.patch(()=> {
    console.log("i am height patch")
})
.delete(()=> {
    console.log("i am height delete")
})