import { Router } from "express";

export let schoolRouter = Router()
schoolRouter
.route("/name/a")  //localhost:8000/school/name/a
.post((req,res,next)=>{
console.log("I am post school")
res.json(req.body)
})

.get((req,res,next)=>{
console.log("I am get school")
res.json({school:"get"})
})

.patch((req,res,next)=>{
console.log("I am patch school")
res.json({school:"patch"})
})

.delete((req,res,next)=>{
console.log("I am delete school")
res.json({school:"delete"})
})