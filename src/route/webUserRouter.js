import { Router } from "express"
import { createWebUser, deleteSpecificWebUser, forgotPasswordWebUser, loginUser, myProfile, readAllWebUser, readSpecificWebUser, updatePassword, updateProfile, updateSpecificWebUser, verifyEmail } from "../controller/webUserController.js"
import { isAuthenticated } from "../middleware/isAuthenticated.js"

export let webUserRouter = Router()
webUserRouter
.route("/")
.post(createWebUser)
.get(readAllWebUser)

webUserRouter
.route("/verify-email") 
.patch(verifyEmail)

webUserRouter
.route("/login")
.post(loginUser)

webUserRouter
.route("/my-profile")
    //middleware    //controller
.get(isAuthenticated, myProfile)

//middleware
//they are function
//which has req, res, next

webUserRouter
.route("/update-profile")
.patch(isAuthenticated, updateProfile)

webUserRouter
.route("/update-password")
.patch(isAuthenticated, updatePassword)

webUserRouter
.route("/forgot-password")
.post(forgotPasswordWebUser)


webUserRouter
.route("/:id")
.get(readSpecificWebUser)
.patch(updateSpecificWebUser)
.delete(deleteSpecificWebUser)