import { Router } from "express";
import { createCollege, deleteCollege, readCollege, readSpecificCollege, updateCollege } from "../controller/collegeController.js";

export let collegeRouter = Router()
collegeRouter
.route("/") //localhost:8000/colleges
.post(createCollege)

//url?query//
.get(readCollege)



collegeRouter
.route("/:id")

.get(readSpecificCollege)
.patch(updateCollege)
.delete(deleteCollege)