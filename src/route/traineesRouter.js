import { Router } from "express";
import { createTrainee, deleteTrainee, readSpecificTrainee, readTrainee, updateTrainee } from "../controller/traineeController.js";

export let traineesRouter = Router()
traineesRouter
.route("/") //localhost:8000/trainees
.post(createTrainee)

//url?query//
.get(readTrainee)



traineesRouter
.route("/:id")

.get(readSpecificTrainee)
.patch(updateTrainee)
.delete(deleteTrainee)

traineesRouter
.route("/:id/a/:name") //localhost:8000/trainees/any/a/any
.get((req,res,next) => {
    console.log(req.body)
    console.log(req.params)
    console.log(req.query)
    res.json({
        success:true,
    })
})

traineesRouter
.route("/:id/a/:name/b/:address")
.post((req,res,next) => {
    console.log(req.params)
    res.json({
        success:true,
    })
})


//o/p
//{ id: '123', name: 'bhawana', address: 'dhumbarahi' }