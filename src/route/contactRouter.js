import { Router } from "express";
import { createContact, deleteContact, getContact, getSpecificContact, updateContact} from "../controller/contactController.js";

export let contactRouter = Router()
contactRouter
.route("/")
.post(createContact)

.get(getContact)

contactRouter
.route("/:id")
.get(getSpecificContact)

.patch(updateContact)

.delete(deleteContact)

