import { Router } from "express";
import { createTeacher, deleteTeacher, readSpecificTeacher, readTeacher, updateTeacher } from "../controller/teacherController.js";

export let teacherRouter = Router()
teacherRouter
.route("/")
.post(createTeacher)
.get(readTeacher)

teacherRouter
.route("/:id")
.get(readSpecificTeacher)
.patch(updateTeacher)
.delete(deleteTeacher)
