import { Router } from "express";
import { createBook, deleteBook, readBook, readSpecificBook, updateBook } from "../controller/bookController.js";

export let BookRouter = Router()
BookRouter
.route("/")
.post(createBook)
.get(readBook)

BookRouter
.route("/:id")
.get(readSpecificBook)
.patch(updateBook)
.delete(deleteBook)
