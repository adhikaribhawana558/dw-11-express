import { Router } from "express";
import { Homework } from "../schema/model.js";

export let homeworkRouter = Router()
homeworkRouter
.route("/")
.post(async (req,res,next)=>{
    let data = req.body
    try {

        let result = await Homework.create(data)
        res.json({
            success: true,
            message:"Student create successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success: false,
            message:error.message
        })
        
    }
})
.get(async (req,res,next)=>{
    try {

        let result = await Homework.findById({})
        res.json({
            success: true,
            message:"Student read successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
        
    }
})

homeworkRouter
.route("/:id")
.get(async (req,res,next)=>{
    let id = req.params.id
    try {

        let result = await Homework.findById(id)
        res.json({
            success: true,
            message:"Student find successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
        
    }
})
.patch(async (req,res,next)=>{
    let id = req.params.id
    let data = req.body
    try {

        let result = await Homework.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success: true,
            message:"Student updated successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
        
    }
})
.delete(async (req,res,next)=>{
    let id = req.params.id
    try {

        let result = await Homework.findByIdAndDelete(id, data,{new:true})
        if (result===null) {
                res.json({
                    success:false,
                    message:"Homework not found"
         })
            
        } else {
            res.json({
                success: true,
                message:"Student deleted successfully",
                result:result,
            })
        }
        

    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
        
    }
})