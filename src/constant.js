import { config } from "dotenv"

config()
export let port = process.env.PORT
export let mongoURL = process.env.MONGO_URL
export let secretKey = process.env.SECRET_KEY
export let user = process.env.USER
export let pass = process.env.PASS