import mongoose from "mongoose"
import { mongoURL } from "../constant.js"

export let connectToMongoDb = ()=>{
    mongoose.connect(mongoURL) //from mongodb
}