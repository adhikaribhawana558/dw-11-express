import { Schema } from "mongoose";

//classRoom=> , name, numberofBench, hasTv
export let classRoomSchema = Schema({
    name:{
        type: String,
        required: [true,"Name field is required"]
    },
    numberOfBench:{
        type: Number,
        required: true,
    },
    hasTV :{
        type: Boolean,  
        required: true,
    },
},{timestamps:true})