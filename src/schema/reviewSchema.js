import { Schema } from "mongoose";

/*   productid
reviewid
descripsion*/
export let reviewSchema = Schema({
    productID:{
        type: Schema.ObjectId,    //Id ko form ma hunu paryo
        ref:"Product", //from model
        required: [true, "productID field is required"],
    },
    userID: {
        type:Schema.ObjectId,
        ref:"User", //from model
        required: [true, "userID field is required"],
    },
    description: {
        type:String,
        required: [true, "description field is required"],
    },
    
  
},
{timestamps:true})