//college=>  name, location,

import { Schema } from "mongoose";

export let collegeSchema = Schema({
    name: {type: String, 
    required: true
    },

    location: {
    type: String,
    required: true,
    },
},{timestamps:true})