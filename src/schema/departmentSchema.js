//department=> name, hod, totalmember

import { Schema } from "mongoose";

export let departmentSchema = Schema({
    name: {
        type: String,
        required: true,
    },
    hod:{
        type: String,
        required:true,
    },
    totalMember:{
        type: Number,
        required: true,
    },
},
{timestamps:true})



