import { Schema } from "mongoose";

export let homeworkSchema = Schema({
    title:{
        type: String,
        required: true,
    },
    description: {
        type:String,
        required: true,
    },
  
},
{timestamps:true})