//defining array is called model
//name
//object

import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { teacherSchema } from "./teacherSchema.js";
import { traineeSchema } from "./traineeSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { classRoomSchema } from "./classRoomSchema.js";
import { departmentSchema } from "./departmentSchema.js";
import { homeworkSchema } from "./homeworkSchema.js";
import { contactSchema } from "./contactSchema.js";
import { hostelSchema } from "./hostelSchema.js";
import { bookSchema } from "./bookSchema.js";
import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";
import { webUserSchema } from "./webUserSchema.js";




export let Student = model("Students",studentSchema)
export let Teacher  = model("Teacher",teacherSchema)
export let Trainee = model("Trainee",traineeSchema)
export let College = model("College",collegeSchema)
export let ClassRoom = model("ClassRoom",classRoomSchema)
export let Department = model("Department",departmentSchema)
export let Homework = model("Homework",homeworkSchema)
export let Contact = model("Contact",contactSchema)
export let Hostel = model("Hostel",hostelSchema)
export let Book = model("Book",bookSchema)
export let Product = model("Product",productSchema)
export let User = model("User",userSchema)
export let Review = model("Review",reviewSchema)
export let WebUser = model("WebUser",webUserSchema)

//model name must be singular