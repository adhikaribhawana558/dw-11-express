// fullName // dob // phonenumber // gender // role -> superAdmin(admin lai delete garni permanently) admin(product halni), customer(product kinni) // isverifiedEmail=>suru ma false link click garesi true // email // password


import { Schema } from "mongoose";

export let webUserSchema = Schema({
    fullName:{
        type: String,
        required: [true, "fullName field is required"],
    },
    dob: {
        type:Date,
        required: [true, "dob field is required"],
    },
    phoneNumber: {
        type:Number,
        required: [true, "phoneNumber field is required"],
    },
    gender: {
        type:String,
        required: [true, "gender field is required"],
    },
    role: {
        type:String,
        required: [true, "role field is required"],
    },
    isVerifiedEmail: {
        type:Boolean,
        required: [true, "isVerifiedEmail field is required"],
    },
    email: {
        type:String,
        required: [true, "email field is required"],
        unique:true,
    },
    password: {
        type:String,
        required: [true, "password field is required"],
    },
  
},{timestamps:true}
) //timestamps:true  auto thapinxa  createAt and updateAt