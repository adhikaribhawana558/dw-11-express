import { Schema } from "mongoose";

//trainee=> name, class, facaulty
export let traineeSchema = Schema({
    name: {
        type: String,
        required: [true, "name field is required"],
    },
    class :{
        type: Number,
        required: [true, "class field is required"],
    },
    faculty:{
        type: String,
        required: [true, "faculty field is required"],
    },
},
{timestamps:true})


