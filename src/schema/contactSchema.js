import { Schema } from "mongoose"

export let contactSchema = Schema({
    fullName:{
        type: String,
        required:[true,"Please provide your Full Name"]
    },

    address:{
        type: String,
        required: [true,"Address is needed"]
    },

    phoneNumber:{
        type:Number,
        required:[true, "phoneNumber is required"]
    },

    email:{
        type:String,
        required:[true,"email is required"],
        unique: true
    },

},{timestamps:true})













// data :{
//     fullName:"nitan",
//     address:"gagalphedi",
//     phoneNumber:9849468999,
//     email:"nitanthapa425@gmail.com"
// }