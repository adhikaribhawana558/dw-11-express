import { Schema } from "mongoose";

export let studentSchema = Schema({
   name:{ //BhaWanA
    type: String,
    required: [true, "name field is required"],
   //  lowercase:true, //manipulation 
   // uppercase:true, //manipulation
   // trim:true, //manipulation
   minLength:[5, "name must be at least 5 character long"], //validation
   // maxLength:[10, "name must be at most 10 character long"], //validation
   //regex=pattern
   validate:(value)=>{  //custom validation   //validate" refers to the process of checking whether a given input adheres to a specified pattern or format. 
      let onlyAlphabet = /^[A-Za-z]+$/.test(value)
      if(onlyAlphabet){
      }
      else{
         throw new Error("Name field must be alphabet")
      } 
   },
   },

   password:{
      type : String,
      required: [true, "password field is required"],
      //password must have 1 number, one lower case, one upper case, one symbol, min 8 character, max 15 character
      validate:(value)=>{
         let validPassword = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[a-zA-Z0-9!@#$%^&*()_+]{8,15}$/.test(value)
         if (validPassword) {
            
         } else {
            throw new Error("Password must be 1 number, one lower case, one upper case, one symbol, min 8 character, max 15 character")
         }
      }
   },

   phoneNumber: {
      type: Number,
      required: [true, "phoneNumber field is required"],
      trim: true,

      validate: (value)=>{
         let strPhoneNumber = String(value)
         if(strPhoneNumber.length === 10){
         }
         else{
            throw new Error("The length of the phone number should be exactly 10")
         }
      }
   },

   gender:{
      type:String,
      default:"female", //manipulation
      required: [true, "gender field is required"],
      validate: (value)=>{
         if(value==="male" || value==="female" || value==="other" ){

         }
         else{
            let error = new Error("Gender must be either male, female and other")
            throw error;
         }
      }
   },

   roll:{
      type : Number,
      required: [true, "roll field is required"],
      min:[50, "roll must be greater than or equal to 50"],//inbuilt validation
      max:[100, "roll must be less than or equal to 100"], //inbuilt validation

   },
   
   isMarried:{
      type : Boolean,
      required: [true, "isMarried field is required"],
   },

   spouseName:{
      type : String,
      required: [true, "spouseName field is required"],
   },

   email:{
      type : String,
      required: [true, "email field is required"],
      // unique: true, //validation
      validate:(value)=>{
         let isValidEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
         // if (isValidEmail) {
         // } else {
         //    throw new Error("Email must be valid")
         // }
         if(!isValidEmail){
            throw new Error("Email must be valid")
         }
      }
   },

   dob:{
      type : Date,
      required: [true, "dob field is required"]
   },

   location:{
      country:{
         type :String,
         required: [true, "country field is required"]
      },
      exactLocation:{
         type :String,
         required: [true, "exactLocation is required"]
      },
   },


},
{timestamps:true})

//validation
//inbuilt validation=> 
// maxlength, minlength,(for string) 
// max n min for number



