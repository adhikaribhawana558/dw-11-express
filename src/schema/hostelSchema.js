import { Schema } from "mongoose";

export let hostelSchema = Schema({
    firstName:{
        type: String,
        required: true,
    },
    lastName:{
        type: String,
        required: true,
    },
    age:{
        type: Number,
        required: true,
    },
    address:{
        type: String,
        required: true,
    },
    email:{
        type:String,
        required: true,
        unique:true,
    },

},{timestamps:true})