import { Schema } from "mongoose";

//book=> name, author ,price, isAvailable
export let bookSchema = Schema({
    name:{
        type: String,
        required: [true, "name field is required"],
    },
    author: {
        type: String,
        required: [true, "author is required"],
    },
    price: {
        type: Number,
        required: [true, "price field  is required"],
    },
    isAvailable: {
        type: Boolean,
        required: [true, "isAvailable field is required"],
    },
},{timestamps:true})