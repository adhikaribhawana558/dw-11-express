import { secretKey } from "../constant.js"
import jwt from "jsonwebtoken"

//isAuthenticated in middleware?=> token valid xa ki nae vnera check garxa
//token valid 
export let isAuthenticated = async(req,res,next)=>{
    try {
         //get token from postman
let tokenString = req.headers.authorization //token get garna ko lagi
let tokenArray = tokenString.split(" ")
let token = tokenArray[1]
//verify token
    let user = await jwt.verify(token, secretKey)
    // let _id = user._id
    req._id = user._id
    next()
    } catch (error) {
        res.status(401).json({
            success: false,
            message: "Token not valid",
        })
        
    }

}