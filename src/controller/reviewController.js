import { Review } from "../schema/model.js"

export let createReview = async(req,res,next)=>{
    let data = req.body
    try {
        let result = await Review.create(data)
        res.json({
            success:true,
            message:"Review created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readReview = async(req,res,next)=>{
    try {
        let result = await Review.find({}).populate("productID").populate("userID")
        res.json({
            success:true,
            message:"Review read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readSpecificReview = async(req,res,next)=>{
    let id = req.params.id

    try {
        let result = await Review.findById(id)
        res.json({
            success:true,
            message:"Review read specific successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let updateReview = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

    try {
        let result = await Review.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"Review updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let deleteReview = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Review.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"doesn't exist"
            })
            
        } else {
            res.json({
                success:true,
                message:"Review deleted successfully",
            })
            
        }
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}