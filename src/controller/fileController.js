export let uploadSingleFile = (req,res,next)=>{
    console.log(req.file)
    res.json({
        success: true,
        message:"File upload successfully",
        link: `http://localhost:8000/${req.file.filename}`
    })
}

export let  uploadMultipleFiles = (req,res,next)=> {
    let link = req.files.map((value, i)=>{
        return `http://localhost:8000/${value.filename}` 	

    })
    res.json({
        success: true,
        message:"File upload successfully",
        link:link
    })
}