import { College} from "../schema/model.js"

export let createCollege= async(req,res,next)=>{
    let data = req.body
    try {
        let result = await College.create(data)
        res.json({
            success:true,
            message:"College created successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })

        
    }
  
}

export let readCollege= async (req,res,next)=>{
    try {
        let result = await College.find({})
        res.json({
            success:true,
            message:"College read successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

   
}

export let readSpecificCollege= async (req,res,next)=>{
    let id = req.params.id
    try {
        let result = await  College.findById(id)
        res.json({
            success:true,
            message:"College read specific successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
        
    }

   
}

export let updateCollege= async (req,res,next)=>{
    let data = req.body
    let id = req.params.id
    try {
        let result = await College.findByIdAndUpdate(id, data, {new:true})
        res.json({
            success:true,
            message:"College update successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message: error.message
        })
        
    }
   
}

export let deleteCollege= async(req,res,next)=>{
    let id = req.params.id
    // let data= req.body
    try {
        let result = await  College.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success: false,
                message: "College doesnot exist"
            })
        } else {
            res.json({
                success: true,
                message:"College deleted successfully",
                result: result,
            })
        }
       
    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
    }
  
} 