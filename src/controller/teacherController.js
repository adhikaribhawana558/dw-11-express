import { Teacher } from "../schema/model.js"

export let createTeacher = async(req,res,next)=>{
    let data = req.body
    try {
        let result = await Teacher.create(data)
        res.json({
            success:true,
            message:"Teacher created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readTeacher = async(req,res,next)=>{
    try {
        let result = await Teacher.find({})
        res.json({
            success:true,
            message:"Teacher read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readSpecificTeacher = async(req,res,next)=>{
    let id = req.params.id

    try {
        let result = await Teacher.findById(id)
        res.json({
            success:true,
            message:"Teacher read specific successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let updateTeacher = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

    try {
        let result = await Teacher.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"Teacher updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let deleteTeacher = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Teacher.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"doesn't exist"
            })
            
        } else {
            res.json({
                success:true,
                message:"Teacher deleted successfully",
            })
            
        }
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}