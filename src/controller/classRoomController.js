import { ClassRoom } from "../schema/model.js"

export let createClassRoom = async(req,res,next)=>{
    let data = req.body
    try {
        let result = await ClassRoom.create(data)
        res.json({
            success:true,
            message:"Classroom created successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readClassRoom = async(req,res,next)=>{
    try {
        let result = await ClassRoom.find({})
        res.json({
            success:true,
            message:"Classroom read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let readSpecificClassRoom = async(req,res,next)=>{
    let id = req.params.id

    try {
        let result = await ClassRoom.findById(id)
        res.json({
            success:true,
            message:"Classroom read specific successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let updateClassRoom = async(req,res,next)=>{
    let id = req.params.id
    let data = req.body

    try {
        let result = await ClassRoom.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"Classroom updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}

export let deleteClassRoom = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await ClassRoom.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"doesn't exist"
            })
            
        } else {
            res.json({
                success:true,
                message:"Classroom deleted successfully",
            })
            
        }
        
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}