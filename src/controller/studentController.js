import { Student } from "../schema/model.js"

export let createStudent = async (req,res,next)=>{
    let data = req.body
    try {

        let result = await Student.create(data)
        res.json({
            success: true,
            message:"Student create successfully",
            result:result,
        })

    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
        
    }

    // console.log(data)
   
}
export let getStudent = async(req,res,next)=>{
    try {
        // let result = await Student.find({}) //MONGOdb ma vko sab data dekhauxa


        // Exact Searching/









                //String searching
        // let result = await Student.find({name:"bhawana"})
        // let result = await Student.find( {name: { $in: [ "bhawana","raman" ] } } )
        //While searching we only focus on value (we don't focus on type)
        // let result = await Student.find({name:"bhawana", roll: 57})


              //Number Searching
        // let result = await Student.find( {roll:50 } ) //roll=50
        // let result = await Student.find( {roll: {$gt:50} } ) //greater than 50
        // let result = await Student.find({roll:{$gte:50}}) //greater than equal to 50
        //let result = await Student.find({roll:{ $lt:50} }) //less than 50
        // let result = await Student.find({roll:{ $lte:50} }) //less than equal to 50
        // let result = await Student.find({roll:{ $ne:50 } }) //not equal to 50
        // let result = await Student .find ({roll:{ $in:[20, 25, 30]}}) //include 20, 25,30
        // let result = await Student.find({roll:{ $gte: 20, $lte: 25}}) //20-25 samma ko data dinxa

        //Non Exact Searching
        // let result = await Student.find({name: {$in:["bhawana","ram"]}})
        //For Regex searching
        // let result = await Student.find({name:"nitan"})
        // let result = await Student.find({name:/nitan/})
        // let result = await Student.find({name:/nitan/i})
        // let result = await Student.find({name:/ni/})
        // let result = await Student.find({name:/ni/i})
        // let result = await Student.find({name:/^ni/})
        // let result = await Student.find({name:/^ni/i})
        // let result = await Student.find({name:/ni$/})

        /*----------------select--------------------------------------*/
        //find has control over the object where as select as control over the object properties
        // let result = await Student.find({}).select("name gender -_id")
        // let result = await Student.find({}).select("name gender")
        // let result = await Student.find({}).select("-name -gender")
        // let result = await Student.find({}).select("name -gender") //it is not valid
        //in select use either all - or use all + but donot use both except _id

     /* -----------------------  sort  ------------------------------*/
    //     let result = await Student.find({}).sort("name")
    //     let result = await Student.find({}).sort("-name")
    //    let result = await Student.find({}).sort("name age")
    //     let result = await Student.find({}).sort("-name age")
    //     let result = await Student.find({}).sort("age -name")


        /*------------------------------skip---------------------------------*/
        // let result= await Student.find({}).skip("3")



        /*-----------------limit---------------------*/
        // let result= await Student.find({}).limit("4")


        // let result= await Student.find({}).skip ("3") . limit ("4")

        //order works
        //find, sort, select, skip, limit

        res.json({
            success: true,
            message:"Student read successfully",
            result: result,
        })
   
   
    } catch (error) {
        res.json({
            success: false,
            message:"Unable to read",
        })
        
    }
  
}
export let getSpecificStudent = async(req,res,next)=>{
    // console.log(req.params)
    let id = req.params.id
    // console.log(id)

    try {
        let result = await Student.findById(id)
        res.json({
            success: true,
            message:"Student read successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success: false,
            message:error.message
        })
        
    }
    
}
export let updateStudent = async(req,res,next)=>{
    let id = req.params.id
    let data= req.body
    try {
        let result = await  Student.findByIdAndUpdate(id, data,{new:true})
        //if{new:false} it gives old data
        //if{new:true} it gives new data
        res.json({
            success: true,
            message:"Student updated successfully",
            result: result,
        })
    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
    }
       
}
export let deleteStudent = async(req,res,next)=>{
    let id = req.params.id
    // let data= req.body
    try {
        let result = await  Student.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success: false,
                message: "Student doesnot exist"
            })
        } else {
            res.json({
                success: true,
                message:"Student deleted successfully",
                result: result,
            })
        }
       
    } catch (error) {
        res.json({
            success: false,
            message:error.message,
        })
    }
  
} 