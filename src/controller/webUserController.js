import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { secretKey } from "../constant.js"
import { WebUser } from "../schema/model.js"
import { sendEmail } from "../utils/sendMail.js"

export let createWebUser = async(req,res,next)=>{
 try {
    let data = req.body
    /*data ={
    "fullName":"Bhawana Adhikari",
    "email":"adhikaribhawana558@gmail.com",
    "password":"Bhawana@12",
    "dob":"1997-07-09",
    "gender":"female",
    "role":"admin",
    "phoneNumber":9860853944
} */
    let hashPassword = await bcrypt.hash(data.password, 10)

    //data ma hash password ra isVerifiedemail thapna paryo
    data = {
        ...data, //sab data
        isVerifiedEmail:false,
        password:hashPassword,
    }
    let result = await WebUser.create(data) 
    //send mail with link
    //generate token
    let infoObj={
        _id:result._id,
    }
    let expiryInfo ={
        expiresIn: "365d",
    }
        let token = jwt.sign(infoObj, secretKey, expiryInfo)
    //link (front end ko)
    await sendEmail({
        from: "no reply <adhikaribhawana558@gmail.com",
        to: [data.email],  //email should be genuine in postman
        subject:"Account created",
        html:`
       <h1>Your account has been created successfully</h1>
       <a href ="http://localhost:8000/verify-email?token=${token}">
       http://localhost:8000/verify-email?token=${token}

       </a>
    `
    })
    
    //send mail

    res.status(201).json({
        success:true,
        message: "User created successfully",
        data:result,
    })
} catch (error) {
    res.status(400).json({
        success:false,
        message:error.message
    })
 }
}

export let verifyEmail = async(req,res,next)=>{
   
    try {
        let tokenString = req.headers.authorization //token pathauna paryo vne
        let tokenArray = tokenString.split(" ")
        let token = tokenArray[1]
        console.log(token)
        // console.log(tokenString.split(" ")[1])
    
        //verify token
            let infoObj = jwt.verify(token, secretKey)//either throw error or verify vyo vne info obj
            let id = infoObj._id //get_id form token
            console.log(infoObj)

            let result = await WebUser.findByIdAndUpdate(id,{
                isVerifiedEmail:true,
            },
            {
                new:true,
            })
            res.status(201).json({
                success:true,
                message:"User verified successfully",
                result:result,
            })
            
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
}

export let loginUser = async (req,res,next)=>{
    try {
        let email = req.body.email
        let password = req.body.password

        let user = await WebUser.findOne({email:email}) //output in array
        //findOne ko output object ma aauxa natra null
        if(user){
            if(user.isVerifiedEmail){
                let isValidPassword = await bcrypt.compare(password, user.password) //Output either true or false
                    if(isValidPassword){
                        let infoObj={
                         _id: user._id,
                
                        }
                        let expiryInfo ={
                            expiresIn: "365d",
                        }
                        let token = jwt.sign(infoObj, secretKey, expiryInfo)

                        res.json({
                            success: true,
                            message: "User login successfully",
                            data:token,
                        })

                }
                else{
                    let error = new Error("Credentials doesn't match")
                    throw error
                }


            }
            else{
                let error = new Error ("Credentials doesn't match")
                throw error
            }

        }
        else{
            let error = new Error("Credentials not found")
            throw error
        }
    } catch (error) {
        res.json({
        success: false,
        message: error.message
        })
    }

}

export let myProfile = async (req,res,next)=>{
    try {
        let _id = req. _id
        let  result = await WebUser.findById(_id)
        res.status(200).json({
            success:true,
            message: "Profile read successfully",
            data:result,
        })
        
    } catch (error) {
        res.status(400).json({
            success:false,
            message:"Unable to read profile"
        })
        
    }

}

export let updateProfile = async(req,res,next)=>{ //sabai update hunxa except email and password
    try {
        let _id = req._id
        let data = req.body
        // console.log(data)
        delete data.email
        delete data.password
        let result =  await WebUser.findByIdAndUpdate(_id,data,{new:true})
        console.log(result)
        res.json({
            success : true,
            message:"Profile updated successfully",
            data:result,
        })
    } catch (error) {
        console.log(error)
        res.status(400).json({
            success:false,
            message:error.message
        })
    }
}

export let updatePassword = async (req,res,next)=>{
    
        let _id = req._id
        let oldPassword = req.body.oldPassword
        let newPassword = req.body.newPassword
        try{
        let data = await  WebUser.findById(_id)
        let hashPassword = data.password
        let isValidPassword = await bcrypt.compare(oldPassword, hashPassword)
        if(isValidPassword){
            let newHashPassword = await bcrypt.hash(newPassword, 10)
            let result = await WebUser.findByIdAndUpdate(_id, 
                {
                    password:newHashPassword
                },
                {
                    new:true
                })
            res.status(201).json({
                success:true,
                message:'Password Updated Successfully',
                data:result

            })

        }
        else{
            let error = new Error("Credentials doesn't match")
            throw error
        }
    } catch (error) {
        res.status(400).json({
            success:false,
        message: error.message
        })
    }

}

export let readAllWebUser = async (req,res,next)=>{
    
    try {
        let result = await WebUser.find({})
        res.status(200).json({
            success : true ,
            message:"All user reads successfully",
            data:result

        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }

}

export let readSpecificWebUser = async (req,res,next)=>{
    let id = req.params.id //postman bata leko so params.id
    try {
        let result = await WebUser.findById(id)
        res.status(200).json({
            success : true ,
            message:"User read successfully",
            data:result

        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }

}

export let updateSpecificWebUser = async(req,res,next)=>{
   
    try {
        let id = req.params.id
        let data = req.body
        delete data.email
        delete data.password
        let result =  await WebUser.findByIdAndUpdate(id,data,{new:true})  
        res.status(201).json({
            success : true ,
            message:"User updated successfully",
            data:result

        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }

}

export let deleteSpecificWebUser = async(req,res,next)=>{
    try {
        let id = req.params.id
        let result =  await WebUser.findByIdAndDelete(id)  
        res.status(200).json({
            success : true ,
            message:"User deleted successfully",
            data:result

        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }  

}

export let forgotPasswordWebUser = async(req,res,next)=>{
    try {
        let email = req.body.email
        let result = await WebUser.findOne({email:email})
        //Output
        //Result:null or //result = (...)

        if (result) {
            let infoObj={            
                _id:result._id,
            }
            
            let expiryInfo ={
                expiresIn: "365d",
            }
            let token = jwt.sign(infoObj, secretKey, expiryInfo)
               //link (front end ko)
    await sendEmail({
        from: "no reply <adhikaribhawana558@gmail.com",
        to: email,  //email should be genuine in postman
        subject:"Reset password",
        html:`
       <h1>Please click given link to reset your password</h1>
       <a href ="http://localhost:8000/reset-password?token=${token}">
       http://localhost:8000/reset-password?token=${token}

       </a>
    `
    })
    res.status(200).json({
        success:true,
        message:"To reset password link has been sent to the email"
    })
        } else {
            res.status(404).json({
                success:false,
                message:"Email doesnot exist"
            })
            
        }
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message
        })
    }  

}


